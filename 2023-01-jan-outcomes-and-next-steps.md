# Executive Summary regarding January 2023.

Nothing new here: given my unhealthy habits to code overnight I've got a sleep deprivation already and I'm working closely with mental health professionals to fix that for last four monthes. 

I want to keep my performance good enough. I want to deliver things in time. I want to implement better `#workwarbalance` habits. I want to grow as an Engineer and obtain Frontend as a secondary profile.

Personal metrics and measurable indicators are already in place. Action plans are composed and are under alignment with my direct supervisors.

### Known risks (mitigation activities are stated [below](https://gitlab.com/pkor-ext/pkor-ext/-/blob/main/2023-01-jan-outcomes-and-next-steps.md#activities-to-support-progress-toward-personal-okrs-and-to-mitigate-the-risks)):
- Performance downgrade for a week or two: new habits require time and energy. Expected value here is 20% downgrade.
- Mood swings and some unfriendly communication with colleagues while establishing new habits.


# Glossary.

`alkoframework` - opinionated markdown-flavored personal predictability toolset.

`Current Cadence` - three weeks period from 2023-01-23 to 2023-02-12.

`IC` - individual contributor.

`Output` - a measurable and categorizable piece of work. Pomodore Technique and some markdown magic are used to precisely track it.

`Previous Cadence` - three weeks period from 2023-01-02 to 2023-01-22.

# Pevious cadence data.

Remote async makes a difference. With no jokes here. For las years I used to have 9-20 focused slots per week and this year in average I make more than 30 focused slots per week.

## Overnight stays outputs

| work type | week 1 | week 2 | week 3 | cadence total output |
| ---       | ---    | ---    | ---    | ---           |
| **total** | **28** | **43** | **41** | **112**       | 
| ---       | ---    | ---    | ---    | ---           | 
| regular   | 24     |  31    |  20    | **75**        | 
| overnight | 4      |  12    |  21    | **37**        | 



## Categorised outputs

| area                   | current output   | curren percentage | target percentage | 
| ---                    | ---              | ---               | ---               |
| **total output**       | **112**          | **100%**          | **100%**          |  
| ---                    | ---              | ---               | ---               | 
| `code` output          | 27               | 24%               | **40%**           | 
| `work` output          | 18               | 16%               | **20%**           |
| `learn` output         | 16               | 14%               | **20%**           | 
| `plan` output          | 40               | 36%               | **10%**           |
| `alkoframework` output | 11               | 10%               | **10%**           | 


# Ideal state of IC work process.

### No late stays. At all. Never again.

### Lunch breaks are long enough so I can regulary visit the house of pain (aka gym)

### Work percentage distribution targets:

- 40% `code` in my IDE or in terminal. Some hacks and workflow improvements lands into this area as well.
- 20% `work` with issues, MRs, epics, objectives and other work_items.
- 20% `learn` either existing tools or new ones. Growth to FullStack role activities land to this area.
- 10% `plan` personal outcomes, define objectives, cunduct `Go Scrum Yourself (c)(tm)` sprint events.
- 10% `alkoframework` maintainance and evolution effort. This is just a short and catchy nickname for my [markdown-flavored predictability framework](https://gitlab.com/nowhere-inc/tech-ninjas/markdown-flavored-predictability-framework). I use it to bring more gamification into my day-by-day activities.

### Delivery pipeline from individual contributor perspective.

The North Star state in this area is: **"Whenever my MR is approved and merged, I verify my work and submit my next MR to the review."**


#### Clarify, Implement, Obtain Mainteiner Approve as an interdependant tracks.

`Review Track` is the only place where I have almost no control:
- review comments and suggestions are addressed with highest prio to speed up time-to-merge;
- MR description contains every possible detail and the pipeline is green when starting review track.

`Implement Track` is where I can control almost every single element:
- questions and blockers are identified on early stages and support requests are composed;
- implementation contains learning codebase, learning processes, trying things, failing, and trying again;
- coding is started right in time to have pipeline moving.

`Clarify Track` is where inputs from the team or other roles are gathered and carefuly assessed:
- discussions are started early to have time for everyone to contribute;
- focus on the next possible work_item, which is super close to be pushed into development;
- replies and thoughts are addreesed in timely manner and with high prio to unblock others.

#### The following health indicators are good to follow.

At the middle of the day (each of them), answer to following statements should be `YES`:
- I have one MR in review.
- I have one MR close to review.
- I have one MR to move forward with.
- My next work_item is clarified.

# Current cadence metrics, health indicators, Objectives and Key Results.

## Personal Objectives and Key Results

### Objective 1: Deliver things in time.

Key Result 1-1: `code` area increase from 24% to 40% of total output

Key Result 1-2: IC pipeline indicators have green status 70% of time (was 0% since tracking were not in place previously)

### Objective 2: Implement better `#workwarbalance` habits.

Key Result 2-1: Extra Effort area decrease from 33% to 5% of total output

Key Result 2-2: Lunch breaks are in place 90% of days (was 0% since tracking were not in place previously)

Key Result 2-3: `alkoframework` automated measurement is implemented for 5 cadence KRs (was 0)

### Objective 3: Take significant steps into Frontend and vue.js.

Key Result 3-1: Three frontend contributions are merged.

Key Result 3-2: Growth category of `learn` area increase from 0% to 15% of output

## Activities to support progress toward personal OKRs and to mitigate the risks

Known risks here are:
- Performance downgrade for a week or two: new habits require time and energy. Expected value here is 20% downgrade.
- Mood swings and some unfriendly communication with colleagues while establishing new habits.

KRs to mitigate Performance Downgrade:
- Key Result 1-1: `code` area increase from 24% to 40% of total output
- Key Result 1-2: IC pipeline indicators have green status 70% of time (was 0% since tracking were not in place previously)

KRs to mitigate Mood Swings impact:
- Key Result 2-1: Extra Effort area decrease from 33% to 5% of total output
- Key Result 2-2: Lunch breaks are in place 90% of days (was 0% since tracking were not in place previously)

#### `#workwarbalance` activities

- Simply left the laptop at the workplace and have no access to work-related things during evenings.
- Turn off slack at 5pm and turn it on at 11am the next day.
- Be at workplace and start work before 11am, prefferably at 9am.
- Finish work before 8pm, prefferably at 6pm.
- Have a lunch break for at least an hour every day, prefferably two hours.
- Keep coffee chats weekly quantity between 2 and 3. Prioritize opportunities.

#### IC pipeline activities

- Post any MR to the review exactly 17 working hours after prev MR is merged.
- Work exactly on 1 (one!!!) issue. No parallel attempts, please.
- No more than 3 open MRs at each point of time assigned to me.
- Health Indicators to track on daily basis:
  - [ ] Exactly one MR in review.
  - [ ] Exactly one MR close to review.
  - [ ] Exactly one MR to move forward with.
  - [ ] My next work_item is clarified.
  - [ ] One slot invested into Growth category of `learn` area

### Availability hours are already posted to my calendar

![weekly view](/assets/ideal-week.png "weekly view")
