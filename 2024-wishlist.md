# B-Day gifts options

| item | details | grabbed by(*) |
| ------------- | ------------- | ------------- |
| paypal | petrokoriakin@gmail.com | not  yet |
| sport-friendly wireless headphones | https://www.expertreviews.co.uk/headphones/1413257/anker-soundcore-life-q30-review | not yet |
| whiteboard | https://allboards-ukraine.com/cart/ [board order](https://gitlab.com/petrokoriakin1/petrokoriakin1/uploads/d5b6088be5231b62f37eeb03125eaad0/board-order.pdf) | not yet |
| dzhemper blue Freedom by Arber | https://arber.ua/dzhemper-cholovchiy-siny-freedom-by-arber-en | not yet |
| Adult Pink Panther Costume, Animal One-Piece, Cartoon Character Outfit | https://www.amazon.com/York-Nordic-Motivator-Walking-Pole/dp/B0CGT23JYW/ | not yet |
| 2 x York Nordic Motivator Walking Pole | https://www.amazon.com/Fun-Costumes-Pink-Panther-Costume/dp/B07ZBPSFRD | not yet |
| 1300ml Large Capacity Water Bottle with Straw | https://www.amazon.com/Capacity-Outdoor-Plastic-Anti-Fall-Drinking/dp/B0B8S9RMDP/ | not yet |
| Mask Off: Masculinity Redefined by JJ Bola | https://www.plutobooks.com/9780745338743/mask-off/ | not yet |
| Книга Дофамінове покоління. Де межа між болем і задоволеннямa | https://www.yakaboo.ua/ua/dofaminove-pokolinnja-de-mezha-mizh-bolem-i-zadovolennjam.html | not yet |
| USB-хаб Real-El type C Multifunction HUB USB 3.0 with HDMI CQ-700 Space Grey (EL123110002) | https://rozetka.com.ua/ua/real_el_el123110002/p273066838/ | not yet |
| Книга PYTHON для дітей. Веселий вступ до програмування | https://www.yakaboo.ua/ua/python-dlja-ditej-veselij-vstup-do-programuvannja.html | not yet |
| Книга Ruby для дітей. Магічний вступ до програмування | https://www.yakaboo.ua/ua/ruby-dlja-ditej-magichnij-vstup-do-programuvannja.html | not yet |
| Книга Світ Софії | https://www.yakaboo.ua/ua/svit-sofii-1846611.html | not yet |
| просто кошти на білу карту | 4441 1111 5537 7074 | not yet |
| Книга Кав'ярня мертвих філософів. Філософське листування для дітей та дорослих | https://www.yakaboo.ua/ua/kav-jarnja-mertvih-filosofiv-filosofs-ke-listuvannja-dlja-ditej-ta-doroslih.html | not yet |
| Каструля Ringel Berlin з кришкою 3.2 л (RG-20013-20) | https://rozetka.com.ua/ua/ringel_rg_20013_20/p69905726/ | not yet |
| Книга Як бажає жінка. Правда про сексуальне здоров'я | https://www.yakaboo.ua/ua/jak-bazhae-zhinka-pravda-pro-seksual-ne-zdorov-ja.html | not yet |
| Marlboro RED 100s Box PACK | https://chamberswineandliquor.com/product/marlboro-100s-box/ | not yet |
| Organize Your Digital Life in Seconds With The PARA Method | https://www.buildingasecondbrain.com/para? | not yet | 
| Premium Taro Recommended for career-focused professional $69/month | https://www.jointaro.com/membership/ | not yet |
| zerotomastery lifetime plan | https://zerotomastery.io/academy/ | not yet |
| Форма для запікання Luminarc Diwali 22 см N3273 | https://rozetka.com.ua/ua/326440558/p326440558/ | not yet |
| Механічний кухонний таймер KITCHEN TIMER сріблясте яйце | https://rozetka.com.ua/ua/415407381/p415407381/ | not yet |



`grabbed by(*)` column represents wether the row have already been booked by someone

` booked(**)` cell value means someone already grabbed this item

# How to grab an item so other guests are aware

Ping me somewhere and ask to mark an item as grabbed 

`OR`

fork the repo and open MR.


# Nova Post details

+380674990373

Корякін Петро Васильович

Київ, Нова Пошта №135

вулиця Гарматна, 26/2, Київ, 03067
