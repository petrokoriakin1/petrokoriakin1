Manchmal stehen sich sogar die zentralen Werte eines Menschen gegenseitig im Weg und im Idealfall unterstützen sie sich. So sind z.B. "Innere Ausgeglichenheit" und "Gefühlsintensität (Leidenschaft)" manchmal nicht vereinbar, und "menschliche Nähe" und "Autonomie (Unabhängigkeit)" passen oft auch nicht zueinander. Das ergibt in unserer beruflichen oder persönlichen Entwicklung dann einen Stau. Und wenn es staut, versuchen wir oft, auf der Verhaltensebene die Dinge wieder ins Lot zu bringen. Doch vergebens. Der effektive Schritt nämlich ist, an den Kern heranzugehen und mit den eigenen Werten zu arbeiten. Wertearbeit ist demzufolge eine grundlegende Aufgabe in Sachen Selbstführung und Selbstmanagement.

### 1. Die derzeit wichtigsten Werte

Unsere Werte verändern sich je nach Lebensumständen und Einflüssen. Wir legen "Wert" auf unterschiedliche Standards, Regeln und Voraussetzungen, je nachdem in welchem Alter, in welcher persönlichen und beruflichen Entwicklungshase wir uns befinden. Sind wenn wir jung und ungebunden, sind uns vielleicht die Werte Flexibilität und Abenteuer wichtig. Steigen wir in unseren ersten Job ein, legen wir vermutlich auch Wert auf Wachstum und Erfolg. Haben wir eine Familie gegründet, werden uns möglicherweise Sicherheit, Gesundheit und Verantwortung wichtig.

Aber mit welchen Werten leben wir momentan? Manche unserer Werte lassen sich leicht erfragen: "Was ist Ihnen wichtig (im Leben, im Beruf etc.)?" Derartige Fragen bieten schon einmal einen guten Zugang zu den eigenen derzeitigen Werten. Oft ist es jedoch schwierig, die eigenen Werte abzufragen: Wir sind es nicht gewohnt, mit unseren Werten zu arbeiten. Sie erscheinen uns abstrakt und sind uns nicht sofort bewusst. Werte zeigen sich jedoch überall in unserer Lebensorganisation. Wir finden sie, indem wir uns folgende Fragen stellen oder am besten stellen lassen:

- "Wie/Womit verbringen Sie Ihre Zeit?"
- "Wofür geben Sie Ihr Geld aus?"
- "Wofür investieren Sie Ihre Energie?"
- "Welche Person ist Ihnen besonders wichtig und welchen Wert repräsentiert sie?"

Die Frage "Warum?" führt ebenfalls zu den Werten und Überzeugungen eines Menschen. Gut geeignet sind auch mehrere "Warum" - Fragen hintereinander. Sie erzeugen beim Befragten eine tiefe Auseinandersetzung mit sich:

- "Warum ...?" 
- "Und warum...?"

#### 1.1 Übung: Werte erfragen

1. Arbeiten Sie bitte in Zweiergruppen.
2. Erfragen Sie die derzeit wichtigsten Werte Ihres/r Partners/in.
3. Benutzen Sie die o. g. Fragen.
4. Schweifen Sie nicht ab, sondern beachten Sie das Frageziel "wichtigste Werte"
5. Notieren Sie die Werte genau so, wie Ihre Partnerin sie benannt hat.
6. Wechseln Sie nach einigen Minuten.

### 2. Meine wichtigsten Werte hierarchisch geordnet

Untersuchungen zufolge treten Werte in Hierarchien auf. Wir haben immer einen höchsten Wert, den wir in unserem momentanen Lebenskontext als den wichtigsten ansehen. Ordnen wir unsere Werte in einer Werte-Hierarchie an, sind abstraktere (Ziel-)Werte höher angesiedelt als konkretere (Mittel-) Werte. Höher angesiedelte Werte rufen in uns einen größeren Motivationssog oder eine tiefere Verbundenheit hervor als konkretere Werte. Hierbei ist es egal, ob Sie diesem Wert verbunden sind, weil Sie ihn schon leben (Verbundenheit durch die positive Erfahrung des Habens oder der Sättigung) oder weil Sie ihn noch nicht haben und sich nach ihm sehnen (Sog durch die Sehnsucht nach dem Wert, durch das Nicht-Haben oder den Mangel).


Ist ein wichtiger Wert für Sie zum Beispiel "Geld", dann sollten Sie sich fragen, was genau eigentlich "Geld" für Sie repräsentiert: Ist es Sicherheit, ist es Freiheit, Genuss, Zuwendung, Liebe etc.? "Geld" ist in diesem Fall eindeutig ein Mittelwert (ein Wert als Mittel zum Ziel). Zielwert ist einer der Werte, den Geld repräsentiert.

#### 2.1 Übung: Werte hierarchisch ordnen
Um herauszufinden, was Ihre eigentlichen stark motivierenden Zielwerte sind, hinterfragen Sie die Werte auf Ihrer Liste mit folgenden Fragen:

1. "Was repräsentiert der Wert xy für mich?"

Oder anders gefragt:

"Was steht hinter dem Wert xy?"



2. Bitte arbeiten Sie zunächst allein, dann zu zweit.



3. Ordnen Sie nun Ihre acht derzeit wichtigsten Werte nach ihrer Wichtigkeit.

Fragen Sie sich: "Welcher dieser Werte ist mir am wichtigsten, welcher am zweitwichtigsten?" etc.

Oder anders gefragt:

"Wenn ich Wert xy nicht haben könnte, dafür aber Wert yz, wäre das in Ordnung?"

4. Tauschen Sie sich kurz mit Ihrem/r Partner/in aus.



### Werteliste

|    |    |    |    |
|:--:|:--:|:--:|:--:|
| Anziehung | Ausgeglichenheit | Achtung | Aktivität |
| Herkunft | Altruismus | Anerkennung | Hingabe |
| Höflichkeit | Ruhe | Ruhm | Ansehen |
| Identität | Individualismus | Jungfräulichkeit | Kameradschaft |
| Bildung | Klugheit | Sinn | Charisma |
| Kompetenz | Demokratie | Konkurrenz | Demut |
| Kreativität | Distanz | Lässigkeit | Disziplin |
| Lebensstil | Ehre | Leidenschaft | Ehrlichkeit |
| Liebe | Sauberkeit | Schönheit | Selbstverwirklichung |
| Sexualität | Sicherheit | Soziale Kontakte | Sparsamkeit |
| Spaß | Spiritualität | Stärke | Tapferkeit |
| Tatkraft | Einfluss | Entwicklung | Macht |
| Toleranz | Menschlichkeit | Treue | Erfolg |
| Mitgefühl | Familie | Mut | Überlegenheit |
| Überzeugung | Freiheit | Nachkommen | Freude |
| Nachsicht | Freundschaft | Nähe | Frieden |
| Natur | Umweltschutz | Unabhängigkeit | Unparteilichkeit |
| Unterstützung | Gastlichkeit | Objektivität | Genuss |
| Offenheit | Geld | Ordnung | Verantwortung |
| Vergnügen | Vernunft | Gerechtigkeit | Persönlichkeit |
| Verständnis | Geschmack | Pflichtbewusstsein | Vertrauen |
| Geselligkeit | Phantasie | Wachstum | Gesundheit |
| Pracht | Wahrheit | Glaube | Pragmatismus |
| Wechsel | Gleichheit | Prestige | Weisheit |
| Glück | Pünktlichkeit | Weitblick | Gute Laune |
| Rechtmäßigkeit | Wertschätzung | Harmonie | Redegewandtheit |
| Heiterkeit | Reichtum | Herausforderung | Religion |
| Respekt | Zärtlichkeit | Zeitlosigkeit | Zugehörigkeit |
| Zusammenarbeit |  |  |  |
