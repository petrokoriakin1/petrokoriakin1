### Welcome to Petro Koriakin's GoSY Sprint 18


#### Sprint Goal

Stale processes are finished

#### Conscious todos

##### Next steps
- please, invest some reasonable time into system design.
- see if The Fearless Organization by Amy Edmondson, the Harvard Business School professor is worth reading
- please, plan to visit Kyiv on 13 May - perfo.
- catch up with LN advisors


##### Push to idea list
- grammarly for gitlab
- learning the design and the frontend
- work items ordering follow up steps
- mastodon PRs
- ALKO duties: review the roadmap, compose alkopitch draft
- please, assess meetups group for ALKO 
- please, assess the lifetime subscription model

##### Done
- please make Obsidian available and actionable
- post details about chatGPT 4 excersise
- post a nice photo to instgrm
- plaese, email Anna and Daryna
- please, post to the twitter: fiverr+onlyfans
- read fiverr email
- perform ALKO duties: finish init MR

##### Raw input

- fiverr-style gamification proposal
- ping Mark with proposal
- ping Roman
- please the followup:
  - Plug and play web framework for the markdown era !
  - https://publishkit.dev/
  - https://discord.gg/XMgVPajeT9

- please, set spacing for the vscode
- please make Obsidian available and actionable
- link dump
  - https://slobodskyi.com/articles/fiverr
  - https://www.youtube.com/watch?v=B0wC5prIV4Y
  - https://www.quora.com/How-much-did-you-earn-on-Fiverr-in-your-first-month
  - https://www.businessinsider.com/freelancing-fiverr-i-made-10k-learned-these-9-best-strategies-2022-11
  - https://www.fiverr.com/support/articles/4404977038481-Seller-Plus-Waiting-List?segment=seller
  - https://learn.fiverr.com/courses/take/online-freelancing-essentials-be-a-successful-fiverr-seller/lessons/7127723-1-welcome-0-34

- see if The Fearless Organization by Amy Edmondson, the Harvard Business School professor is worth reading
- please, plan to visit Kyiv on 13 May - perfo.
- catch up with LN advisors


- please, invest some reasonable time into system design.
- please, learn: upwork process:
  - read the guide
  - compose action plan
- compose an offer to Volodymyr, and Olena.

- perform ALKO duties:
  - finish init MR
  - add historical data
  - quantify cadences
  - review the roadmap
  - compose alkopitch draft
- please submit a gig to fiver so people can pay you.
- please, create an async hackathon playground retro issue.
- plaese, email Anna and Daryna
- please, assess meetups group for ALKO 
- please, assess the lifetime subscription model
- please, post to the twitter: fiverr+onlyfans
- read fiverr email
- make an lakopitch deck
- post details about chatGPT 4 excersise
- post a nice photo to instgrm

  - perform ALKO duties:
    - add historical data
    - quantify cadences
    - update idea list 
    - review the roadmap

- ignore ALKO duties
- ignore GoSY duties

- perform ALKO duties
- perform GoSY duties
- estimate all issues and MRs


#### Cheatsheet


```
Pomodoro #0
=======


Result
-------


Plan
-------


Details
-------



git commit --allow-empty -m "resume progress"

git add . ;git commit -am "focusbox goal: "
git commit -am "finish focusbox: DONE - 3"
git commit -am "finish focusbox: FAILED - 3"



git commit --allow-empty -m "start lunchbreak"
git commit --allow-empty -m "finish lunchbreak"

git log --format="%an (%ad): %h %s" --reverse -n 50

git commit --allow-empty -m "pause progress"
git add .; git commit -am "pause progress"; git push
```