GoSY Daily, 22 May 2023
=======
- Sprint 23'21 Goal: Fail two or three SYstem Design interviews 

What did I do yesterday?
-------
No idea tbh (-; 

What will I do today?
-------

a. What is my goal for today?

Submit a test assignment solution

b. What are the things I plan to do to achieve this goal?
  - Write some code
  - Compose an email and submit the solution as is
  - Deploy things if possible
  - survive another therapy session

c. In case I have unexpected life processes and I can invest only 75 minutes of focused time, the most valuable actions will be:
  - Compose an email and submit the solution as is

What are blockers or obstacles that I see?
-------

Nothing special to mention here.



Today I achieved and learned
-------


Outputs and results of the day
-------


Extra items
-------

Pomodoro #7
=======
implement !3


Result
-------
- expectations for the service

Plan
-------
- expectations for the service
- implement basic



Pomodoro #6
=======
implement !3


Result
-------
- finish specs for seeds
- extract the service

Plan
-------
- finish specs for seeds
- extract the service


Details
-------

Pomodoro #5
=======
implement !3


Result
-------
- split seeds into different files
- and specs for seeds

Plan
-------
- split seeds into different files
- and specs for seeds


Details
-------


Pomodoro #4
=======
implement !3


Result
-------
- introduce seeds

Plan
-------
- introduce seeds and specs for seeds


Details
-------
- please, check if my rubocop workaround for bullet actually works.
- please, set default spacing for the things.

Pomodoro #3
=======
implement !3


Result
-------
- introduce UUIDs


Plan
-------
- introduce UUIDs
- introduce seeds and specs


Details
-------


Pomodoro #2
=======
implement !3

Result
-------
- add simple spec
- make it green


Plan
-------
- add simple spec
- add rake seeds
- make it green

Details
-------
- please, check if bullet works with sqlite3 somehow
- check abilities gem - https://github.com/varvet/pundit
- check GUID best practices
- check seeds best practices, please


Pomodoro #1
=======
work: test assignment

Result
-------
- create a super PDF
- compose an email update.

Plan
-------
- create a super PDF
- compose an email

Details
-------


