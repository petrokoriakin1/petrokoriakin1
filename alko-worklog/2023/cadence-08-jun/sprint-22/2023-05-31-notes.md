GoSY Daily, 30 May 2023
=======
- Sprint 23'22 Goal: 

What did I do yesterday?
-------


What will I do today?
-------

a. What is my goal for today?


b. What are the things I plan to do to achieve this goal?
  - 
  - 
  - 

c. In case I have unexpected life processes and I can invest only 75 minutes of focused time, the most valuable actions will be:
  - 

What are blockers or obstacles that I see?
-------

Nothing to mention here.



Today I achieved and learned
-------


Extra items
-------


Outputs and results of the day
-------

Pomodoro #2
=======
implement !9

Result
-------
- finish smart specs: presenter logic
- make specs green:
  - classes
  - models (partialy)

Plan
-------
- finish smart specs: presenter logic
- make specs green:
  - classes
  - models
  - factories
  - services

Details
-------
```ruby
# models
group.steward = random_participant


# factories
let(:group) { create(:group, :with_members) }
let(:steward) { group.members.samle }
subject(:group) { build(:group, :with_members, :with_steward)}

# services
CreateDefaultActivityWithUsers

# controllers
Presenters
``` 





Pomodoro #1
=======
implement !9

Result
-------
- add some smart specs

Plan
-------
- add some smart specs
- make them green

Details
-------
```ruby
# models
group.steward = random_participant


# factories
let(:group) { create(:group, :with_members) }
let(:steward) { group.members.samle }
subject(:group) { build(:group, :with_members, :with_steward)}

# services
CreateDefaultActivityWithUsers

# controllers
Presenters
``` 



