# GoSY Daily, 24 Jun 2023

## Sprint 23'25 Goal: Release Random Activities


## Outcomes, achievements and learnings from yesterday
- communication and collaboration eats my time. 16:32 till 23:16 is too much.

## Extra items from yesterday
- ETA doc to keep up to date - https://docs.google.com/spreadsheets/d/1r8w9WYPQ3Q6M5qy_dImwLC5oNlD2EcjBUJOvt-DsKXI/edit?usp=sharing
- finsish the bookshelf construction effort

## What will I do today to achieve the Sprint Goal?

### What is my goal for today?
- move JSON API forward. min 10 items

### What are the things I plan to do to achieve this goal?
- morning focusbox
- lunch
- evening focusbox


### In case I have unexpected life processes and I can invest only 75 minutes of focused time, the most valuable actions will be:
- lunch

### What are blockers or obstacles that I see?
- 



# Outputs and results of the day


Pomodoro #0
=======
title

Plan
-------
- 
- 

Result
-------
- 
- 

Details
-------
