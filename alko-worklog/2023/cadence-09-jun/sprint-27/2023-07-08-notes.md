# GoSY Daily, 08 Jul 2023

## Sprint 23'27 Goal: 


## Outcomes, achievements and learnings from yesterday
- 

## Extra items from yesterday
- 

## What will I do today to achieve the Sprint Goal?

### What is my goal for today?
- 

### What are the things I plan to do to achieve this goal?
-


### In case I have unexpected life processes and I can invest only 75 minutes of focused time, the most valuable actions will be:
- 

### What are blockers or obstacles that I see?
-



# Outputs and results of the day




Pomodoro #3
=======
work: maptask

Plan
-------
- add few dependenciew
  - rubocop
  - rspec
  - postgis
- render some map

Result
-------
- add few dependencies
  - rubocop
  - rspec
  - postgis

Details
-------
- no chance to reuse active record things

Pomodoro #2
=======
work: maptask

Plan
-------
- start a simple docker app
- read through the docs

Result
-------
- start a simple docker app
- read through the docs

Details
-------
- should I start with [docker](https://github.com/nickjj/docker-rails-example/tree/main)
  - yes, let's do that.
- should I use [reek](https://github.com/troessner/reek) for code smell detections from the very beginning?
  - no. maybe later

Pomodoro #1
=======
work: maptask

Plan
-------
- read the guide and add notes
- follow the tutorial

Result
-------
- read the guide and add notes
  - https://github.com/rgeo/rgeo
  - https://github.com/rgeo/activerecord-postgis-adapter
- follow the tutorial

Details
-------
- should I start the docker from the very beginning
  - yes, I should.

- `rails new rails-postgis-leaflet -j esbuild -d postgresql -c tailwind --skip-test --skip-bundle --skip`
- створити нове поле (намалювавши його контур на карті);
- змінити назву поля та контур поля; 
- відкрити сторінку одного поля та 
- вивести його на карті - localhost:3000/fields/1
- вивести таблицю всіх полів та 
- відобразити всі поля на карті. - localhost:3000/fields
