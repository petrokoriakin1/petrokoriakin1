# GoSY Daily, 09 Jul 2023

## Sprint 23'27 Goal: 


## Outcomes, achievements and learnings from yesterday
- some viable steps toward maps

## Extra items from yesterday
- make thigs work with rails 7
  - [leaflet.js on Rails 7 with esbuild + stimulus.js](https://knerav.medium.com/rails-7-with-esbuild-stimulus-js-leaflet-js-f6de933cfdd)
  - [TiSer/field_map](https://github.com/TiSer/field_map/)
  - [Working with PostGIS data in Leaflet: Part 1 2018](https://www.youtube.com/watch?v=U_x8KBCge0w)
  - [Leaflet Draw Polygon | Create, Edit, Delete Geometrics | Leaflet-Draw Plugin | GeoDev 2022](https://youtu.be/vK_-OI3pjXk)
  - [leaflet Repurpose EditToolbar.Edit handler to enable Multipolygon editing.](https://jsfiddle.net/47m6ycw9/)
  - [tutorialspoint LeafletJS - Multi Polyline and Polygon](https://www.tutorialspoint.com/leafletjs/leafletjs_multi_polyline_and_polygon.htm)
  - [Leaflet Draw API reference](https://leaflet.github.io/Leaflet.draw/docs/leaflet-draw-latest.html)

## What will I do today to achieve the Sprint Goal?

### What is my goal for today?
- make a maptask complete

### What are the things I plan to do to achieve this goal?
- recreate things
- make tests
- call about docker


### In case I have unexpected life processes and I can invest only 75 minutes of focused time, the most valuable actions will be:
- recreate things

### What are blockers or obstacles that I see?
- sunday's unvilling to make steps



# Outputs and results of the day

