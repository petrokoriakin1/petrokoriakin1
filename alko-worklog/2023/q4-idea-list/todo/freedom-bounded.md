# The list of the ideas I implement right after I have enough freedom.

## [3-5 pomodorros per month] DataCamp learnings
- [Форма для участі тут](https://docs.google.com/forms/d/e/1FAIpQLSds-wwNvVd5V69XJIHAwIpHPoscEgFCpsd3I42MuzSdGM7_GQ/viewform)
- [Для тих, хто не знає, що таке DataCamp](https://www.datacamp.com/)

## [10-20 pomodorros per week] Rust bootcamp
- [Навчальний Табір по Rust - Зима 2023-2024](https://docs.google.com/forms/d/e/1FAIpQLSd_vLXn6cCEUWQdYbR70N4OY1JXfllB2e0yk-Cl0ov14EQQqw/viewform)

## [10-20 pomodorros] devops talk
- [Шукаємо класні практичні доповіді. Якщо ви хочете виступити, щоб поділитися своїми знаннями, заповніть, будь ласка, форму до 17 січня 👉 Подати доповідь](https://docs.google.com/forms/d/e/1FAIpQLScnKkH4KflPuknHg2G0191BgiIyQpa2z58IK9TTfDRp0FDWrw/viewform)

## [10-20 pomodorros] softskills talk
- [Шукаємо класні практичні доповіді. Якщо ви хочете виступити, щоб поділитися своїми знаннями, заповніть, будь ласка, форму до 22 лютого 👉 Подати доповідь](https://docs.google.com/forms/d/e/1FAIpQLSc7W71NKgvDa9K87QhGjv_B-T7n4XhFQgCcplAkUK6vSCVguA/viewform)