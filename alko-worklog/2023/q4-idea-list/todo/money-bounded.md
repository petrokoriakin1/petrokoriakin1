# The list of the ideas I implement right after I have enough funds.

## $50: book bundle for AI and software development
  - https://www.humblebundle.com/books/software-development-pearson-books Learn all about the latest methodologies and technologies that are reshaping the world of software development with this book bundle from Pearson. In the Async-First Playbook, you’ll glean invaluable insights on remote-native, asynchronous work styles to prime your globe-spanning teams for success. Serverless as a Game Changer will teach you how to evolve your tech stack to deliver software faster, better, and at lower cost, backed up by real-world case studies. The latest edition of Software Architecture in Practice provides guidance on how to design, instantiate, analyze, manage, and evolve complex software projects, using the latest strategies and tech, like DevOps and quantum computing. Get all these books and more, and help support Women Who Code with your purchase!
  - https://www.humblebundle.com/books/machine-learning-ai-and-bots-oreilly-books Delve deep into the realms of machine learning, AI, and bots with this bundle of books from O’Reilly! Inside this 14-book library, you’ll get expert insights on the tools and technology behind these cutting-edge fields. Learn about coding in Python or TensorFlow. Dig into designing deep learning systems. Level up your skills & knowledge whether you’re a novice or a seasoned prof, and help support Code for America with your purchase!

## 2345 UAH: book a session with [Andrew Nikishaev](https://www.linkedin.com/in/creotiv/)

## 6,661.77 Ukrainian hryvnia: book a [Quarterly Review](https://calendly.com/jenshein/quarterly-review)

To create an intentional life it is important to make regularly set goals and make plans - but also to check if you actually achieved these goals.

That's what the Quarterly Review is there for.

Especially if you are working as a One-Person-Business it might be tricky sometimes to make some time for strategical thinking, identifying improvements, making and evaluating plans.

If you want to do this at least once per quarter in a fast and efficient way - this is exactly your event.
