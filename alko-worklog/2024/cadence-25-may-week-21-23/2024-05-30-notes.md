# ALKO Daily, 30 May 2024

## Cadence 24'25 Goal: Test drive the economy dimension


## Outcomes, achievements and learnings from yesterday
- 

## Extra items from yesterday
- 

## What will I do today to achieve the Cadence Goal?

### What is my goal for today?
- set the quarter to be up and running

### What are the things I plan to do to achieve this goal?
- cancel MIRO
- validate the ideas around:
    - cancel YNAB
    - cancel YouTube
    - cancel domains?
    - cancel GSuite?

### In case I have unexpected life processes, the most valuable actions will be:
- 

### What are blockers or obstacles that I see?
-



# Outputs and results of the day
