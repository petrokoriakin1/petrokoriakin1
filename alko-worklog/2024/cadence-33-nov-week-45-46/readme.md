# Welcome to Petro Koriakin's 2024 ALKO Cadence 33



## Cadence Goal


## Conscious todos

### Next steps

### Push to Idea List

### Done

### Raw input


# Cheatsheet

```
git commit --allow-empty -m "resume progress"

git add . ;git commit -am "focusbox goal: "
git commit -am "finish focusbox: DONE 2"
git commit -am "finish focusbox: FAILED 2"

git log --format="%an (%ad): %h %s" --reverse -n 50

git commit --allow-empty -m "pause progress"
git add .; git commit -am "pause progress"; git push


git add 2023/q4-idea-list/todo ; git commit -m "extend plan"
git add 2023/q4-idea-list ; git commit -m "update done"
```


```
focusbox #1
=======
enjoy: ALKO cadence planning

Plan
-------
- put a goal
- refine the structure

Result
-------
- put a goal

Details
-------
```



No problem at all! Let’s focus the message on your intention to step up as the new organizer, highlight the importance of live events, and encourage participation next Thursday. Here’s a revised version:

Subject Line: 🚀 Exciting News: Let’s Save & Energize Our Kyiv NVC Community!

Dear NVC Practitioners of Kyiv,

I’m thrilled to share that I’m stepping up to take the lead for our Greater Social Life Union of Kyiv Meetup group! This community has been a cornerstone for connection and growth, and I’m committed to ensuring it thrives with regular live events that truly bring us together.

Why Live Events Matter
In-person gatherings allow us to deepen our practice, foster genuine relationships, and build a strong support network. These meetings are where NVC comes alive—not just as a skill but as a shared experience. Let’s continue this tradition and make it even stronger.

Join Us Next Thursday!
Our first live event under this new chapter will take place:

📅 When: Next Thursday
📍 Where: [Insert Venue Details]
🎯 What: A focused NVC practice session, community sharing, and brainstorming about future events.

Your presence and ideas will shape the direction of this group. Together, we’ll plan meaningful sessions, workshops, and discussions that cater to everyone’s needs and interests.

How You Can Support

Show Up: Let’s make next Thursday a powerful kick-off.
Bring Ideas: Share your vision for events you’d love to see.
Contribute: Whether hosting a session, helping with logistics, or simply participating actively, your engagement makes all the difference.
Let’s Keep the Momentum Going!
This community is a shared effort, and it thrives with everyone’s involvement. I’m excited to collaborate and grow alongside all of you.

Feel free to reach out if you have questions or ideas before Thursday. Let’s make this a vibrant new chapter for Greater Social Life Union of Kyiv!

Warm regards,
[Your Full Name]
[Your Contact Information]

Does this version align with your intentions? Let me know if you’d like to emphasize anything further!

Here’s the revised organizer announcement, incorporating your case study to emphasize the value of compassion and second chances while inviting participation in the event:

Subject Line: 🌟 Let’s Revive Our NVC Community—Join Us Thursday!

Dear NVC Practitioners of Kyiv,

I’m stepping up as the new organizer of the Greater Social Life Union of Kyiv. This group has been a beacon of connection and growth, and I believe it has the potential to become even more impactful.

In my work with people facing challenges like addiction, I’ve learned the power of second chances and the transformative effect of understanding. One candidate I supported years ago struggled with personal difficulties but delivered exceptional results. Despite setbacks and criticism, I defended their potential. Today, they’ve turned their life around and continue to thrive. This experience taught me the irreplaceable value of patience, compassion, and belief in others.

Our community embodies these values. Live events give us the space to connect deeply, practice NVC, and build the kind of trust and understanding that fosters growth—for ourselves and others.

Join Us Next Thursday!
📅 When: Thursday, [Insert Date]
📍 Where: [Insert Venue Details]
🎯 What: Focused NVC practice, sharing stories, and shaping our future events.

Let’s come together to create a safe, supportive space for everyone to grow. Your presence and ideas are essential to this new chapter for Greater Social Life Union of Kyiv.

Warm regards,
[Your Name]
[Your Contact Information]

This ties your personal story into the values of NVC, highlighting the importance of live events for fostering meaningful connections. Let me know if further adjustments are needed!

