# YNAB -> money transfers -> savings

## Short term goal

- Have clear budget for upcoming week
- Have clear budget for upcoming month
- Have two months of net savings

## Long term goal

- Have six months of net savings
- Pay back da loans: 14k

## da process:

- YNAB on daily basis
- age my money
- spend 1.5k monthly
- transfer all da money I have into savings
  - Lara 23.5k
  - Maria 30k
  - Masha 64k
- maintain impulsivity budget for 10% of available savings monthly 11k
