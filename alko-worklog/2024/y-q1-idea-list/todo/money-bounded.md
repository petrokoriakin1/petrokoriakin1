# The list of the ideas I implement right after I have enough funds.

## 2345: book a session with [Andrew Nikishaev](https://www.linkedin.com/in/creotiv/)

## 5888: book a [Quarterly Review](https://calendly.com/jenshein/coaching-60)

To create an intentional life it is important to make regularly set goals and make plans - but also to check if you actually achieved these goals.

That's what the Quarterly Review is there for.

Especially if you are working as a One-Person-Business it might be tricky sometimes to make some time for strategical thinking, identifying improvements, making and evaluating plans.

If you want to do this at least once per quarter in a fast and efficient way - this is exactly your event.
