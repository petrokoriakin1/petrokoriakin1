
 
 
 Membership invitation_statuses: pending/active/declined/disabled enum :invitation_status, { pending: "pending", active: "active", disabled: "disabled" }
 when a user creates a new organization, he (the creator) automatically has the "active" membership status
 Assign pending status on membership invitation;
 Notify user that he was invited to an org (MembershipMailer)
 User can see his pending invitations in /organizations; user can accept or decline an invitation
 Admin can mark a members invitation_status as active or disabled
 Only active memberships have access to an organization resources
 Tests!!!

