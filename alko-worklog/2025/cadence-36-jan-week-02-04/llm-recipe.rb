require 'anthropic'
require 'pry'

class RecipeGenerator
  def initialize(access_token)
    @client = Anthropic::Client.new(access_token: access_token)
  end

  def generate_recipe(ingredients)
    prompt = "Create a detailed step-by-step recipe using these ingredients: #{ingredients.join(', ')}"
    binding.pry
    response = @client.messages(
      parameters: {
        model: "claude-3-5-haiku-20241022",
        max_tokens: 1000,
        messages: [
          {
            role: "user",
            content: prompt
          }
        ]
      }
    )

    response.dig('content', 0, 'text')
  rescue StandardError => e
    "Error generating recipe: #{e.message}"
  end
end

# Usage example
access_token = ENV['ANTHROPIC_API_KEY']
generator = RecipeGenerator.new(access_token)
ingredients = ['chicken', 'rice', 'broccoli', 'garlic']
recipe = generator.generate_recipe(ingredients)
puts recipe
