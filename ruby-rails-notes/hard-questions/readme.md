# OOD

1. SOLID - вміти пояснити, щл значить із прикладами;

[Чому SOLID — важлива складова мислення програміста. Розбираємося на прикладах з кодом](https://dou.ua/lenta/articles/solid-principles/)
```
жорсткість (Rigidity): кожна зміна викликає багато інших змін;
крихкість (Fragility): зміни в одній частині ламають роботу інших частин;
нерухомість (Immobility): код не можна повторно використати за межами його контексту.

Single Responsibility Principle
Кожен клас повинен виконувати лише один обов’язок.

Open/Close Principle
Класи мають бути відкриті до розширення, але закриті для змін. 

Liskov Substitution Principle
Якщо об’єкт базового класу замінити об’єктом його похідного класу, то програма має продовжувати працювати коректно.


Interface Segregation Principle
Краще, коли є багато спеціалізованих інтерфейсів, ніж один загальний. Маючи один загальний інтерфейс, є ризик потрапити в ситуацію, коли похідний клас логічно не зможе успадкувати якийсь метод. 

Dependency Inversion Principle
складається з двох тверджень:

високорівневі модулі не повинні залежати від низькорівневих. І ті, і ті мають залежати від абстракцій;
абстракції не мають залежати від деталей реалізації. Деталі реалізації повинні залежати від абстракцій.
```
2. Dependency Injection vs Dependency Inversion;
3.  OOD patterns
4. композиція vs наслідування
5. Архітектурні патерни
6. Load Balancer алгоритм

# DB

1. Рівні ізоляці
    The four standard isolation levels are:
    `Read Uncommitted`: This is the lowest level of isolation where a transaction can see uncommitted changes made by other transactions. This can result in dirty reads, non-repeatable reads, and phantom reads.
    `Read Committed`: In this isolation level, a transaction can only see changes made by other committed transactions. This eliminates dirty reads but can still result in non-repeatable reads and phantom reads.
    `Repeatable Read`: This isolation level guarantees that a transaction will see the same data throughout its duration, even if other transactions commit changes to the data. However, phantom reads are still possible.
    `Serializable`: This is the highest isolation level where a transaction is executed as if it were the only transaction in the system. All transactions must be executed sequentially, which ensures that there are no dirty reads, non-repeatable reads, or phantom reads.
2.  що таке транзакуція в базі
3. якою вона повинна бути (атомарна, і ще там якась)
4. ACID
[what is acid](https://database.guide/what-is-acid-in-databases/)
```
ACID Definition
ACID is an acronym that stands for Atomicity, Consistency, Isolation, Durability. These are explained below.

Atomicity
Atomicity means that you guarantee that either all of the transaction succeeds or none of it does. You don’t get part of it succeeding and part of it not. If one part of the transaction fails, the whole transaction fails. With atomicity, it’s either “all or nothing”.

Consistency
This ensures that you guarantee that all data will be consistent. All data will be valid according to all defined rules, including any constraints, cascades, and triggers that have been applied on the database.

Isolation
Guarantees that all transactions will occur in isolation. No transaction will be affected by any other transaction. So a transaction cannot read data from any other transaction that has not yet completed.

Durability
Durability means that, once a transaction is committed, it will remain in the system – even if there’s a system crash immediately following the transaction. Any changes from the transaction must be stored permanently. If the system tells the user that the transaction has succeeded, the transaction must have, in fact, succeeded.
```



# Ruby

1. Принципи ООП в рубі; (насдіування - приховуваня - поліморфізм (Duck typing))
2. protected vs private
3. send vs public_send
4. proc, block, lambda;
5. Оце знати [https://medium.com/whynotio/shorthand-syntax-for-each-map-reduce-more-a790ba12edc9](https://medium.com/whynotio/shorthand-syntax-for-each-map-reduce-more-a790ba12edc9)
6. map vs each
7. Enumerator vs Enumerable
8. include vs extend
9.  method_missing
10. respond_missing?
11. як шукаються методи в рубу; (Eigenclass)
12. метапрограмування - що використовував, які плюси - мінуси
13. які типи змінних;
14. пояснити що роблять attr_reader/setter/getter - як можна без них зробити те саме;
15. GIL - згалаьно вміти пояснити що це таке
16. thread-fiber-mutex - загально вміти пояснити
17. Структури дани в Ruby
18. Mutability [https://launchschool.medium.com/variable-references-and-mutability-of-ruby-objects-4046bd5b6717](https://launchschool.medium.com/variable-references-and-mutability-of-ruby-objects-4046bd5b6717) 

# RAILS

1. канкаренсі vs паралелізм
2. типи джойнів;
3. STI
4. habtm vs has_many through
5. includes vs joins
6. різниця між having та where
7. що таке REST ; які екшени будуть у звичайному круді від рельсів
8. resources vs resource
9. яка різниця між scope та методом класу (я хз)
10. n+1 та як уникнути
11. MVC
12. як працюють міграції, як воно знає яку ранити
13. як би ти міняв дані у базі (рейк таск - ок відповідь)
14. які тести пишеш - чим користуєшся
15. як тестив би зовнішню апі, якщо vcr юзаєш то які недоліки такого
16. що таке контракт тести чи писав такі
17. DRY - KISS - YAGNI - не питали але давніше питали - можна прочитати просто
18. OWASP
19. Rack (Middleware)