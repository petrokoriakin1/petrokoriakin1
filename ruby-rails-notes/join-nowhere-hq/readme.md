# A brief guideline how to join

1. Create a profile with readme repo. It's a super simple process and here are some cool examples:
https://gitlab.com/alan
https://gitlab.com/petrokoriakin1
https://gitlab.com/mushakov
https://gitlab.com/zillemarco


2. Request Developer Access to the groups:
- https://gitlab.com/gitlab-community/community-members
- https://gitlab.com/community-coders
- https://gitlab.com/nowhere-inc
- https://gitlab.com/groups/softserve6

3. Enjoy!
